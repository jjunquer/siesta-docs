.. _background:

Background information
======================

History
----------------

..
    This section for history of the project

.. toctree::
    :maxdepth: 0
       
    history


Philosophy
------------------

.. toctree::
    :maxdepth: 1

    Basis sets <basis-sets>
    Pseudopotentials <pseudopotentials>


