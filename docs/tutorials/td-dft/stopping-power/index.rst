:sequential_nav: next

..  _tutorial-td-dft-stopping-power:


MOVING ATOMS: stopping power for H moving along a channel in Ge
===============================================================

In this example we will see how to simulate the dynamics of an
energetic hydrogen atom moving through a channel in Germanium.

The input file hydrogen-in-Ge.0.fdf contains a cell with 8 Germanium
atoms in the fcc phase, and an interstitial Hydrogen. It initializes
the TD-DFT calculation by writing a hydrogen-in-Ge.TDWF and a
hydrogen-in-Ge.TDXV files

Edit this latter file to define an initial velocity on H along
the (001) direction. Remember that XV files contain the atomic
positions and velocities in Bohrs and Bohrs/fs. For example, the last
line should read::

  2     1       0.000000000       1.320400000       0.000000000          0.000000000       0.000000000      20.000000000

where the H atom is set to move with 20Bohr/fs along the z axis.

Now we can start the dynamic evolution of the system. The file
hydrogen-in-Ge.move.fdf defines the simulation parameters::

  MD.TypeOfRun               TDED
  MD.FinalTimeStep            500
  TDED.TimeStep               1.0000000000E-03 fs
  TDED.Nsteps                 10


We save the total energy of the system (and the eigenvalues) at each
time-step, and the final wavefunction using these lines::

  TDED.Write.Etot               T     
  TDED.WF.Save                  T     
 
Finally, note that:

* We are moving ONLY the H atom, keeping frozen the atomic positions of Ge atoms in the lattice::

    %block GeometryConstraints
    position from  1 to 8
    %endblock GeometryConstraints

* Due to the periodic boundary conditions, when the H atom leaves the
  simulation box, another periodic image automatically enters again in
  the cell. Larger supercells would be needed to avoid over-heating
  the system.

After the simulation, we can plot the total energy, which shows the
lost of energy due to the electron heating induced by the fast moving
H atom. The periodic oscillations are due to the host lattice
periodicity. You can repeat the example using different simulation
parameters for the time-propagation, but also for the initial
conditions of the moving H atom.


