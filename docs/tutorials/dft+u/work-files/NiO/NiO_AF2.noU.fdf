#
# General System descriptors
#

SystemName      Nickel Oxide Antiferromagnetic Phase
                                     # Descriptive name of the system
                                     # Experimental coordinates taken from
                                     #    N. G. Schmahl, J. Barthel, and
                                     #    G. F. Eikerling
                                     #    Z. Anorg. Allg. Chem.
                                     #    332, 230 (1964)
SystemLabel            NiO_AF2_noU   # Short name for naming files

NumberOfAtoms           4            # Number of atoms
NumberOfSpecies         2            # Number of species

%block Chemical_Species_Label
  1   28    Ni
  2    8     O
%endblock Chemical_Species_Label

%block PS.lmax                
   Ni  3
    O  3
%endblock PS.lmax

PAO.EnergyShift     0.002 Ry
PAO.BasisSize DZP

#
# Lattice, coordinates, k-sampling
#

LatticeConstant     4.17 Ang

%block LatticeVectors
 1.00     0.50      0.50
 0.50     1.00      0.50
 0.50     0.50      1.00
%endblock LatticeVectors

AtomicCoordinatesFormat Fractional      # Format for coordinates
%block AtomicCoordinatesAndAtomicSpecies
 0.000   0.000   0.000  1   # Atom 1: Ni (Atomic species number 1)
 0.500   0.500   0.500  1   # Atom 2: Ni (Atomic species number 1)
 0.250   0.250   0.250  2   # Atom 3: O  (Atomic species number 2)
 0.750   0.750   0.750  2   # Atom 4: O  (Atomic species number 2)
%endblock AtomicCoordinatesAndAtomicSpecies

%block kgrid_Monkhorst_Pack
  4   0   0   0.5
  0   4   0   0.5
  0   0   4   0.5
%endblock kgrid_Monkhorst_Pack

#
# DFT, Grid, SCF
#

XC.functional           GGA         # Exchange-correlation functional type
XC.authors              PBE         # Particular parametrization of xc func
ElectronicTemperature   0.075 eV    # Electronic Temperature for smearing
                                    #   the Fermi-Dirac distribution
MeshCutoff              400 Ry      # Equivalent planewave cutoff for the grid
MaxSCFIterations        100         # Maximum number of SCF iterations per step
DM.MixingWeight         0.10        # New DM amount for next SCF cycle
DM.Tolerance            1.d-4       # Tolerance in maximum difference
                                    # between input and output DM
DM.NumberPulay          3           # Number of SCF steps between pulay mixing
SCF.MixAfterConvergence .false.

SpinPolarized           .true.      # Spin polarized calculation

# For an AFM-Type II structure, choose the following
%block DM.InitSpin       # Describe the initial magnetic order (on Ni only)
 1   + 
 2   - 
%endblock DM.InitSpin

#
# Options for saving/reading information
#
DM.UseSaveDM            .false.      # Use DM Continuation files
MD.UseSaveXV            .false.      # Use stored positions and velocities

#
# Output options 
#
WriteMullikenPop  1
%block PDOS.kgrid_Monkhorst_Pack 
30  0  0  0.5
 0 30  0  0.5
 0  0 30  0.5
%endblock PDOS.kgrid_Monkhorst_Pack
%block ProjectedDensityOfStates
 -25.0  10.0  0.150  3000   eV
%endblock ProjectedDensityOfStates

WriteIonPlotFiles     .true.

## To plot the fat bands
#COOP.Write              .true.    # Instructs the program to generate
#                                  # SystemLabel.fullBZ.WFSX
#                                  #   (packed wavefunction file)
#                                  # and SystemLabel.HSX (H, S and X ij file),
#                                  # to be processed by Util/COOP/mprop and
#                                  # Util/COOP/fat and
#                                  # to generate COOP/COHP curves,
#                                  # (projected) densities of states,
#                                  # fat bands, etc.
#WFS.Write.For.Bands     .true.    # Instructs the program to compute and
#                                  # write the wave functions associated to the
#                                  # bands specified
#                                  # (by a BandLines or a BandPoints block)
#                                  # to the file SystemLabel.bands.WFSX.
#WFS.band.min            1         # Specifies the lowest band index of the
#                                  # wave-functions to be written to the file
#                                  # (in this context) SystemLabel.fullBZ.WFSX
#                                  # for each k-point
#                                  # (all k-points in the BZ sampling
#                                  # are affected).
#WFS.band.max            20        # Same as before, but for the
#                                  # highest band index
#

BandLinesScale      ReciprocalLatticeVectors
%block BandLines
      1  0.000       0.000      0.000     \Gamma
     40  0.500       0.000      0.000     L
     20  0.81250     0.34375    0.34375   K
     40  0.500       0.500      0.500     T
     40  0.000       0.000      0.000     \Gamma
     40  0.500       0.500      0.000     X
%endblock BandLines

#
# Molecular dynamics and relaxations
#

MD.TypeOfRun       CG               # We are going to perform a
                                    #   Conjugate-Gradient minimization
MD.VariableCell    .true.           # Is the lattice relaxed together with
                                    #   the atomic coordinates?
MD.NumCGsteps       0               # Number of CG minimization steps for
                                    #   cell optimization
MD.MaxStressTol    0.0010 eV/Ang**3 # Tolerance in the maximum
                                    #   stress in a MD.VariableCell CG optimi.

%block GeometryConstraints          # Constraints impossed on
   position   1   2   3   4         #   the atomic positions
%endblock GeometryConstraints
WriteCoorStep





