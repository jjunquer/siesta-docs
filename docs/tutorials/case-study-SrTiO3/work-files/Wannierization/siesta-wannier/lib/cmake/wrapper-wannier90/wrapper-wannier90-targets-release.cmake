#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "wrapper-wannier90::wrapper-wannier90-lib" for configuration "Release"
set_property(TARGET wrapper-wannier90::wrapper-wannier90-lib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(wrapper-wannier90::wrapper-wannier90-lib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "Fortran"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libwrapper-wannier90-lib.a"
  )

list(APPEND _cmake_import_check_targets wrapper-wannier90::wrapper-wannier90-lib )
list(APPEND _cmake_import_check_files_for_wrapper-wannier90::wrapper-wannier90-lib "${_IMPORT_PREFIX}/lib/libwrapper-wannier90-lib.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
