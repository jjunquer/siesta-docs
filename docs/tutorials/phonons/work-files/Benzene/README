*******************************************************************************
WARNING: BEFORE RUNNING A CALCULATION FOR PRODUCTION,
TEST THE PSEUDOPOTENTIAL AND BASIS SETS, AND PERFORM
THE CONVERGENCE TESTS (MESH CUTOFF, ETC.) 

IN THE PRESENT EXAMPLES, AND IN ORDER TO SPEED THE CALCULATIONS,
WE PROVIDE SOME VALUES OF THESE PARAMETERS FOR YOU.

WE DO NOT WARRANTY THAT THE VALUES OF THESE PARAMETERS ARE CONVERGED.

THE RESULTS PRESENTED BELOW HAVE BEEN OBTAINED USING:
Executable      : siesta
Version         : 5.1-MaX-41-g6ad11e234-dirty
Architecture    : x86_64
Compiler version: GNU-12.3.0
Compiler flags  : -fallow-argument-mismatch -O3 -march=native
Parallelisations: MPI
NetCDF support
NetCDF-4 support
NetCDF-4 MPI-IO support
Lua support
DFT-D3 support
Wannier90 wrapper support

RESULTS MIGHT DIFFER SLIGHTLY DEPENDING ON THE PLATFORM, COMPILER,
AND COMPILATION FLAGS
*******************************************************************************

All the lines after the $ should be written by the user in the command line.

* The goal of this exercise is to compute the vibrational frequency of
  a molecule (benzene).

* Before running the calculation to compute the vibrational frequencies,
  the first step is to relax the geometrical structure of the system under
  study. 

  So, to start with, we run a conjugate gradient minimization to 
  relax the atomic positions. 
  The input file has been prepared for you in the file benzene.relax.fdf.
  See how the structure of the benzene molecule has been introduced
  in the Z-matrix format (especification of internal variables,
  such as distances, angles, and torsional angles).
  This allows the minimization including some constraints in the symmetry
  in a trivial way.

$ siesta < benzene.relax.fdf |tee  benzene.relax.out

* Edit the file benzene.relax.out and search for the relaxed structure

outcoor: Relaxed atomic coordinates (Ang):                  
    2.50440754    5.00000000    0.00000000   2       1  C
    3.20381827    6.21141492   -0.00000000   2       2  C
    4.60263973    6.21141492   -0.00000000   2       3  C
    5.30205046    5.00000000   -0.00000000   2       4  C
    4.60263973    3.78858508   -0.00000000   2       5  C
    3.20381827    3.78858508   -0.00000000   2       6  C
    1.39580443    5.00000000   -0.00000000   1       7  H
    2.64951671    7.17149338   -0.00000000   1       8  H
    5.15694129    7.17149338   -0.00000000   1       9  H
    6.41065357    5.00000000   -0.00000000   1      10  H
    5.15694129    2.82850662   -0.00000000   1      11  H
    2.64951671    2.82850662   -0.00000000   1      12  H


zmatrix: Z-matrix coordinates: (Ang ; deg )
zmatrix: (Fractional coordinates have been converted to cartesian)
molecule    1 (    12 atoms)
      2.50440754      5.00000000      0.00000000
      1.39882146     90.00000000     60.00000000
      1.39882146    120.00000000     90.00000000
      1.39882146    120.00000000      0.00000000
      1.39882146    120.00000000      0.00000000
      1.39882146    120.00000000      0.00000000
      1.10860311    120.00000000    180.00000000
      1.10860311    120.00000000      0.00000000
      1.10860311    120.00000000      0.00000000
      1.10860311    120.00000000      0.00000000
      1.10860311    120.00000000      0.00000000
      1.10860311    120.00000000      0.00000000

------------------------------------------------------------------------------
HOW TO COMPUTE THE FORCE CONSTANT MATRIX IN REAL SPACE
------------------------------------------------------------------------------

*  We prepare an input file, called benzene.ifc.fdf
   to run Siesta and compute the interatomic force
   constant in real space. 

   Remember that, for a molecule, we do not require to build any supercell
  
   For this, just copy the relaxed coordinates and unit cell from
   the benzene.XV generated after the relaxation to the 
   AtomicCoordinatesAndAtomicSpecies block. 
   Do not forget to include the atomic masses after the coordinates 
   of each atom.
  
   Define also the unit cell lattice vectors.

   We have prepared that file for you. It is name is benzene.ifc.fdf
   Take a look to:

LatticeConstant     1.0 Bohr
%block LatticeVectors
   21.938124322       0.000000000       0.000000000
    0.000000000      20.556799916       0.000000000 
    0.000000000       0.000000000      11.910755412
%endblock LatticeVectors

AtomicCoordinatesFormat NotScaledCartesianBohr
%block AtomicCoordinatesAndAtomicSpecies
    4.732644351       9.448630623       0.000000000     2    12.0107
    6.054339081      11.737873049      -0.000000000     2    12.0107
    8.697728542      11.737873049      -0.000000000     2    12.0107
   10.019423273       9.448630623      -0.000000000     2    12.0107
    8.697728542       7.159388198      -0.000000000     2    12.0107
    6.054339081       7.159388198      -0.000000000     2    12.0107
    2.637688094       9.448630623      -0.000000000     1     1.00794
    5.006860953      13.552158387      -0.000000000     1     1.00794
    9.745206671      13.552158387      -0.000000000     1     1.00794
   12.114379530       9.448630623      -0.000000000     1     1.00794
    9.745206671       5.345102860      -0.000000000     1     1.00794
    5.006860953       5.345102860      -0.000000000     1     1.00794
%endblock AtomicCoordinatesAndAtomicSpecies

*  To compute the interatomic force constant in real space, we have
   to run Siesta

$ siesta < benzene.ifc.fdf | tee benzene.ifc.out 

*  The interatomic force constant matrix in real space are stored
   in a file called SystemLabel.FC
 
   Again, the explanation of the different entries of this file can
   be found in the theoretical lectures.

------------------------------------------------------------------------------
HOW TO COMPUTE THE DYNAMICAL MATRIX AT THE GAMMA-POINT,
AND THE PHONON EIGENFREQUENCIES AND EIGENVECTORS
------------------------------------------------------------------------------

*  Once the interatomic force constants in real space have been computed,
   a discrete Fourier transform is performed to compute the dynamical matrix
   in reciprocal space. 
   Then, the dynamical matrix is diagonalized and its eigenfrequencies and
   eigenvectors are computed.
   This is done using the vibra code.

   In the case of a molecule, only the Gamma point is relevant.
   It is specified in the same way as to compute the electronic
   band structure, in the same file benzene.ifc.fdf

Eigenvectors    .true.        # Compute both phonon eigenvalues and eigenvectors
BandLinesScale  pi/a
%block BandLines
1   0.0   0.0   0.0   \Gamma  # Only the Gamma point (enough for a molecule)
%endblock BandLines

   To compute the vibrational frequencies
 
   
$ vibra < benzene.ifc.fdf > vibra.out

*   The output of this code is:

    SystemLabel.bands: with the different mode frequencies (in cm^-1).
    They are stored in the same way as the electronic band structure.

    SystemLabel.vectors: with the eigenmodes at Gamma
    (the format is self-explained).

------------------------------------------------------------------------------
HOW TO VISUALIZE THE PHONONS
------------------------------------------------------------------------------

*   After getting the .vectors file (calculated by vibra) 
    and the .XV file (computed in Siesta),
    run the vib2xsf program.

    You have to answer a few question on the fly,
    regarding the name of the files where the .vectors are stored,
    the units to be used to introduce the lattice vectors (Bohrs or Angstroms),
    the zero of coordinates,
    the unit cell lattice vectors,
    the first mode to visualize,
    the last mode to visualize,
    the amplitude of the modes to be visualized, and
    the number of steps in the movie.

    You can play a little bit, but to save time we have prepared
    all the answers in the file vib2xsf for you.

    Just run

$ vib2xsf 

    And answer the questions with the corresponding information (line by line) 
    provided in the file vib2xsf.dat

    This will produce two files per mode:

    .XSF file: contains a static structures (as in .XV), 
    with arrors added to each atom to indicate displacement pattern.

    .AXSF file: contains the animation of a phonon, for a (user-chosen) 
    amplitude and number of steps.

    They can be visualized using XCRYSDEN

$ xcrysden

    Select "File"
    Open Structure
    Open AXSF (Animation XCrySDen Structure File)
    
    And then select you file and type OK.

    The same can be done to visualize the XSF file, but just chosing
    Select "File"
    Open Structure
    Open XSF file (XCrySDen Structure File)
    
    


