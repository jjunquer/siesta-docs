\contentsline {section}{\numberline {1}PREFACE}{2}
\contentsline {section}{\numberline {2}A PRIMER ON AB-INITIO PSEUDOPOTENTIALS}{2}
\contentsline {section}{\numberline {3}COMPILING THE PROGRAM}{3}
\contentsline {section}{\numberline {4}USING THE ATOM PROGRAM}{3}
\contentsline {subsection}{\numberline {4.1}All-electron calculations}{3}
\contentsline {subsection}{\numberline {4.2}Pseudopotential generation}{5}
\contentsline {subsubsection}{\numberline {4.2.1}Core Corrections}{7}
\contentsline {subsection}{\numberline {4.3}Pseudopotential test}{8}
\contentsline {section}{\numberline {5}APPENDIX: THE INPUT FILE}{11}
\contentsline {section}{\numberline {6}APPENDIX: INPUT FILE DIRECTIVES}{16}
