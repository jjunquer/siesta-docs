:sequential_nav: next

..  _tutorial-fatbands:

Fat-bands for Si
----------------

Here we will use the ``fat`` program to compute the projections of
wavefunctions obtained from band-structure calculation with the option
``Write.WFS.For.Bands`` onto specified orbital sets. The resulting
"fatbands" can later be plotted.

.. hint::
   Enter the *si_fatbands* directory

Here we see a si_fatbands.fdf file with a slightly modified
band-structure section::

  BandLinesScale  pi/a
  WFS.Write.For.Bands T             # For fat-bands analysis
  Wfs.band.min 1
  Wfs.band.max 8
  %block BandLines                  # These are comments
  1  0.000  0.000  0.000  \Gamma   # Begin at Gamma
  25  2.000  0.000  0.000     X     # 25 points from Gamma to X
  10  2.000  1.000  0.000     W     # 10 points from X to W
  15  1.000  1.000  1.000     L     # 15 points from W to L
  20  0.000  0.000  0.000  \Gamma   # 20 points from L to Gamma
  25  1.500  1.500  1.500     K     # 25 points from Gamma to K
  %endblock BandLines

Note the ``WFS.*`` options, requesting the output of wavefunctions for
specific bands. Without them, the program would only generate a .bands
file with band eigenvalues.  Note also::

  Save-HS T

Once we run the file, we will have  si_fatbands.HSX
and si_fatbands.bands.WFSX files, and we can process them::

   ln -sf si_fatbands.bands.WFSX si_fatbands.WFSX
   fat [options] fatbands

using the orbital sets in the *fatbands.mpr* file.

Then, a number of .EIGFAT files will be produced. These files contain
*both* the eigenvalues and the projection weight for each orbital set
in the .mpr file, and can be re-processed by the "eigfat2plot" program
(in Util/Bands in the Siesta distribution) to produce data files
suitable for plotting by Gnuplot::

  eigfat2plot fatbands.fatbands_Si_3s.EIGFAT > 3s.dat
  eigfat2plot fatbands.fatbands_Si_3p.EIGFAT > 3p.dat
  eigfat2plot fatbands.fatbands_Si_3d.EIGFAT > 3d.dat

The typical commands with gnuplot are::

  plot "3s.dat" using 1:2:(4*$3) with points pt 6 ps variable  
  replot "3p.dat" using 1:2:(4*$3) with points pt 6 ps variable 

to tag the orbital projections with different visual markers.

Alternatively, users might want to produce their own post-processors.





