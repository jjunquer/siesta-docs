:sequential_nav: next

..  _tutorial-aiida:

AiiDA Siesta
------------

AiiDa siesta is a python package to perform high-throughput simulations with siesta.

The tutorial is hosted in the aiida-siesta documentation and can be found at this 
`link <https://docs.siesta-project.org/projects/aiida-siesta/en/latest/tutorials/siesta-school-2021.html>`_
