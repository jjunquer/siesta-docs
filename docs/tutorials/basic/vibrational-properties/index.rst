:sequential_nav: next

..  _tutorial-basic-vibrational-properties:

Vibration modes and phonons
===========================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

.. note::
   For background, see the
   excellent `slide presentation <https://drive.google.com/file/d/1SRhSOWwFVPmS3vr4REJSvqgXf2Tic8fl/view?usp=sharing>`_
   by Andrei Postnikov. 
   
In this set of exercises we will use the method of finite-differences
implemented in Siesta to compute force constants in real space. We
will explore the cases of a molecule and of a crystal. In the latter
case we will focus on the need of a supercell to represent the
real-space force constants.

We will also try the visualization tools available.

   
.. toctree::
    :maxdepth: 1

    Benzene/index
    Si-bulk/index
   

   
    
	       
   
   

   
  
  
   

   
