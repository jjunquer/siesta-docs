:sequential_nav: next

..  _tutorial-basic-pseudopotentials:

Pseudopotentials
================

For more information on actually generating and testing a pseudopotential, see
:ref:`this 'intermediate' tutorial<tutorial-pseudopotentials>`.

Even if we do not worry about creating the pseudopotential, we cannot
really treat it completely as a black box. In this short tutorial,
still to be finalized, we will discuss some basic concepts of the
use of pseudopotentials in Siesta, and particular aspects to take
into account.
     
* Types of pseudopotential files and where to find them.

  * Traditional PSF pseudopotentials.
  * Newer PSML pseudopotentials. The Pseudo-Dojo database.
  
* Core/valence configuration of the pseudopotential and its influence
  on basis-set generation.

  * Heuristics
  * Handling of semicore states.
  
* The Kleinman-Bylander transformation and its options. Ghosts.

* Does the pseudopotential affect the real-space cutoff needed in
  Siesta?
  
  
   
