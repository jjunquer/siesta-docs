*******************************************************************************
WARNING: BEFORE RUNNING A CALCULATION FOR PRODUCTION,
TEST THE PSEUDOPOTENTIAL AND BASIS SETS, AND PERFORM
THE CONVERGENCE TESTS (MESH CUTOFF, ETC.) 

IN THE PRESENT EXAMPLES, AND IN ORDER TO SPEED THE CALCULATIONS,
WE PROVIDE SOME VALUES OF THESE PARAMETERS FOR YOU.

WE DO NOT WARRANTY THAT THE VALUES OF THESE PARAMETERS ARE CONVERGED.

THE RESULTS PRESENTED BELOW HAVE BEEN OBTAINED USING THE ATOM PROGRAM
THAT COMES WITH THE VERSION OF SIESTA
Compiler: g95 (version 0.92)
Compilation flags: FFLAGS= -O2 -Wall
                   FPPFLAGS= -DGRID_DP $(FPPFLAGS_CDF) $(FPPFLAGS_MPI)
Double precision for the grid variables enabled.
No linear algebra libraries used. 
RESULTS MIGHT DIFFER SLIGHTLY DEPENDING ON THE PLATFORM, COMPILER,
AND COMPILATION FLAGS
*******************************************************************************

All the lines after the $ should be written by the user in the command line.

******************************************************************************
Generating a virtual atom usung the virtual crystal approximation
******************************************************************************

* The goal of this example is to generate the pseudopotential for a 
virtual atom with a non-integer number of electrons.
This is done mixing pseudopotentials for different atoms.

* To compile the code that generates the mixed pseudopotential,
go to the Util/VCA directory and simply tape

$ cd ~/siesta/Util/VCA
$ make

It will use the same arch.make file as for the compilation of Siesta
(no need to copy it again to the Util/VCA directory)

* This will generate two executable files:

mixps		 (program to mix pseudopotentials)
fractional	 (program that multiplies the strength of a pseudopotential by a 		given fraction)

* To run the example, change to the directory where the files are
included:

$ cd $PATH/atom/Tutorial/PS_Generation

* Create a directory where the mixing will be performed

$ mkdir VCA-O-F
$ cd VCA-O-F

* Copy in this directory the two pseudopotentials that will be mixed
(they have been previously generated as explained in the corresponding 
exercise)

$ cp ../O/O.tm2.psf ./O.psf
$ cp ../F/F.tm2.psf ./F.psf

* Mix the pseudopotentials. Assuming that the path to the Siesta directory
is ~/siesta, then

$ ~/siesta/Util/VCA/mixps O F 0.9

The two first arguments after mixps correspond to the labels of the two 
atoms involved, while the third argument is the mixing parameter.
In this example, 90% of the first element and 10% of the second one.

In this particular example a virtual with a charge of 6.1 electrons 
is generated:
0.9 * 6 (electrons in O) + 0.1*7 (electrons in F) = 
6,1 electrons in the Virtual Atom
(2.0 electrons in the s chanel and 4.1 electrons in the p channel)

* The new pseudopotential is stored in a file whose name is 
the symbol of the two original pseudopotentials, 
followed by the mixing parameter (up to five decimal places)
OF-0.9000.psf in this case.

* The new block to be included in the Siesta .fdf file to generate the 
neutral atom potential is stored in the file:
OF-0.90000.synth

