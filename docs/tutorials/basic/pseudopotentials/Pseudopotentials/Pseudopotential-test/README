*******************************************************************************
WARNING: BEFORE RUNNING A CALCULATION FOR PRODUCTION,
TEST THE PSEUDOPOTENTIAL AND BASIS SETS, AND PERFORM
THE CONVERGENCE TESTS (MESH CUTOFF, ETC.) 

IN THE PRESENT EXAMPLES, AND IN ORDER TO SPEED THE CALCULATIONS,
WE PROVIDE SOME VALUES OF THESE PARAMETERS FOR YOU.

WE DO NOT WARRANTY THAT THE VALUES OF THESE PARAMETERS ARE CONVERGED.

THE RESULTS PRESENTED BELOW HAVE BEEN OBTAINED USING THE ATOM PROGRAM
THAT COMES WITH THE VERSION OF SIESTA
Compiler: g95 (version 0.92)
Compilation flags: FFLAGS= -O2 -Wall
                   FPPFLAGS= -DGRID_DP $(FPPFLAGS_CDF) $(FPPFLAGS_MPI)
Double precision for the grid variables enabled.
No linear algebra libraries used. 
RESULTS MIGHT DIFFER SLIGHTLY DEPENDING ON THE PLATFORM, COMPILER,
AND COMPILATION FLAGS
*******************************************************************************

All the lines after the $ should be written by the user in the command line.

******************************************************************************
Testing a norm-conserving pseudopotential for N
******************************************************************************

* The goal of this example is to test the transferability of 
  a norm-conserving pseudopotential for N, 
  using the reference configuration chosen in the previous example:
- Neutral N:                          1s2, 2s2, 2p3

  A pseudopotential with good transferability will reproduce the
  all-electron energy levels and wavefunctions in arbitrary
  environments, (i.e., in the presence of charge transfer, which always
  takes place when forming solids and molecules).  We know that norm
  conservation guarantees a certain degree of transferability (usually
  seen clearly in the plot of the logarithmic derivative), but we can
  get a better assessment by performing all-electron and ``pseudo''
  calculations on the same series of atomic configurations, and comparing
  the eigenvalues and excitation energies.


* To run the example, change to the directory where the files are
  included:

$ cd ../atom/Tutorial/PS_Generation/N

* The input file for the pseudopotential test has been prepared for you,
  and it is called N.test.inp.
  It is made by the concatenation of nine jobs for all-electron ({\tt ae})
  calculations and nine jobs for the pseudopotential test ({\tt pt}) 
  for the same configurations:

  Configuration                       
-------------------------------------------------------------------------------
- Neutral N: 2s2, 2p3, 3d0, 4f0 
- 2s1.8 2p3.2 (promotion of 0.2 electrons from the s channel to the p channel).
- 2s1.6 2p3.4 (promotion of 0.4 electrons from the s channel to the p channel).
- 2s1.4 2p3.6 (promotion of 0.6 electrons from the s channel to the p channel).
- 2s1.2 2p3.8 (promotion of 0.8 electrons from the s channel to the p channel).
- 2s1.0 2p4.0 (promotion of 1.0 electrons from the s channel to the p channel).
- 2s2.0 2p2.0 3d1.0 (promotion of 1.0 electrons from the p channel to the d). 
- 2s2.0 2p2.0 (removing one electron from the p channel, ionic conf. +1).
- 2s2.0 2p3.7 (adding 0.7 electrons to the p channel, ionic conf. -0.7).


* Examine in detail the input file.
  More details can be found in Docs/atom.pdf
  and in the powerpoint file that comes with this and previous examples

* Run a pseudopotential test calculation for this reference configuration

$ ../../Utils/pt.sh N.test.inp N.tm2.vps

  where the first file is the name of the input file and the second the 
  pseudopotential that is going to be tested (in unformatted .vps format)

* This will generate a directory where the different
  output files are dumped (the name of the directory is the concatenation
  of the name of the input file and the name of the pseudopotential file,
  both without the .inp and the .vps extensions).
  In our case N.test-N.tm2

  A detailed explanation of the different files can be found
  in the ATOM User's Guide, page 6.

* We can compare the AE and PS eigenvalues simply typing

$ cd N.test-N.tm2
$ grep '&v' OUT | grep s

  (and similarly for $p$, $d$, and $f$, if desired). 

  They should be identical by construction for the reference configuration.
  Here, they are not 
  exactly identical because the pseudopotentials are changed slightly 
  to make them approach their limit tails faster.

  The typical difference should be of around 
  1 mRyd for a ``good'' pseudopotential.
  (The real proof of good transferability, remember, can only come
  from a molecular or solid-state calculation). Note that the PT levels
  are labeled starting from principal quantum number 1.
  
* We can also compare the difference in total energies between the
  AE and PS configurations simply typing

$ grep "&d" OUT

  The tables (top AE, bottom PT) give the cross-excitations among all
  configurations. Typically, one should be all right if the AE-PT
  differences are not much larger than 1 mRy.

* The radial shape of the all electron (AE) and
  pseudo test (PT) valence charge densities,
  angularly integrated (that is, multiplied by 4 \pi r^2)
  are given in the files CHARGE and PTCHARGE.

  You can compare them using the script charge as follows:

$ gnuplot -persist charge.gplot
     (to generate a figure on the script using gnuplot)
$ gnuplot charge.gps
     (to generate a postscript file with the figure)

  The PS and AE valence charge densities must be equal beyond the largest
  cutoff radii.

  What is the origin of the peak observed around 0.2 bohrs from the origin
  in the AE valence charge density?.

* The shape of the valence all-electron (AE) and pseudo-(PT) wave-functions 
  can be plotted using the scripts 

$ gnuplot -persist pt.gplot
     (to generate a figure on the script using gnuplot)
$ gnuplot pt.gps
     (to generate a postscript file with the figure)

 (Note: in versions of gnuplot more recent that 4.4,
 it is possible that you will have to edit the pt.gplot and pt.gps files
 and replace
 set data style lines
 by
 set style data lines)

 
