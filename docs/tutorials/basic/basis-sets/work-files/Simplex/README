*******************************************************************************
WARNING: BEFORE RUNNING A CALCULATION FOR PRODUCTION,
TEST THE PSEUDOPOTENTIAL AND BASIS SETS, AND PERFORM
THE CONVERGENCE TESTS (MESH CUTOFF, ETC.)

IN THE PRESENT EXAMPLES, AND IN ORDER TO SPEED THE CALCULATIONS,
WE PROVIDE SOME VALUES OF THESE PARAMETERS FOR YOU.

WE DO NOT WARRANTY THAT THE VALUES OF THESE PARAMETERS ARE CONVERGED.

THE RESULTS PRESENTED BELOW HAVE BEEN OBTAINED USING:
Version of Siesta: trunk-432 (serial mode)
Compiler: g95 (version 0.92)
Compilation flags: FFLAGS= -O2 -Wall
                   FPPFLAGS= -DGRID_DP $(FPPFLAGS_CDF) $(FPPFLAGS_MPI)
Double precision for the grid variables enabled.
No linear algebra libraries used.
RESULTS MIGHT DIFFER SLIGHTLY DEPENDING ON THE PLATFORM, COMPILER,
AND COMPILATION FLAGS
*******************************************************************************

All the lines after the $ should be written by the user in the command line.

Example basis optimization for the water molecule.
For the sake of simplicity, only the split-norm parameters will be optimized.
Global soft-confinement potential will be used for 
all the atomic shells of all the atoms. 
The values of the prefactor and the inner radius are given in the variables

PAO.SoftDefault    T           # Global soft-confinement options
PAO.SoftPotential 50.0 Ry
PAO.SoftInnerRadius 0.70

* The input file in this example is called TEMPLATE. As you see, there
 some input variables do not receive any particular value, but are kept
 as variables to be optimized with the Simplex algorithm. 

 These variables are easy to idendity: they have a leading '$'.
 ($spl_O or $spl_H).

* TEMPLATE and VARS define the optimization problem. 
 Be careful to match the names that you use in the TEMPLATE 
 (without leading '$') with
 those in the VARS file.  
 DO NOT leave any line blank in VARS (not even at the end).

 In VARS, the three numbers after the name of the variable are:
 their ranges of variability (first: minimum, second: maximum).
 Optionally, a third number can be used to specify the
 starting value. If this starting value is missing, a random starting point
 in the appropriate range is chosen.

* Look in PARAMS to set the appropriate values of the parameters which
 influence the Simplex process (it uses a two-level approach, with
 amoeba-like iterations followed by periodic restarts with new
 simplex hyper-tetrahedra of progressively smaller sizes. The stopping
 criteria are a fractional energy tolerance (compared to the differences
 between the highest and lowest energies of the vertices) and a minimum
 fractional size for the hyper-tetrahedron.

* To compile the optimizer code, and assuming that the Siesta package 
 is installed in ~/siesta, simply type

 $ cd ~/siesta/Util/Optimizer
 $ make

 This will compile the simplex using the same arch.make file 
 as the one you to compile siesta

* The run_script.sh script simply drives Siesta to perform the calculations.
 You need at least siesta-2.1.46.

 To run the optimization, and assuming that the Siesta package is installed
 in ~/siesta, simply type

 $ ~/siesta/Util/Optimizer/simplex > simplex.out

 (You might want to put all this in a queuing-system script...)

* At the end of a successful minimization, the optimized values of the
 parameters can be found in a file called final.sed,

 $ more final.sed
 s/$spl_O/.1007813/g
 s/$spl_H/.5671875/g

 or at the end of the output file after running the Simplex

 $ tail -f simplex.out
 Lambda <  0.01 . Convergence presumably reached
 Final (average) point:
 Final (average)spl_O : 0.09453125000000007
 Final (average)spl_H : 0.5703124999999999
 Final (actual minimum):
 Finalspl_O : 0.10078125000000007
 Finalspl_H : 0.5671874999999998



