*******************************************************************************
WARNING: BEFORE RUNNING A CALCULATION FOR PRODUCTION,
TEST THE PSEUDOPOTENTIAL AND BASIS SETS, AND PERFORM
THE CONVERGENCE TESTS (MESH CUTOFF, ETC.) 

IN THE PRESENT EXAMPLES, AND IN ORDER TO SPEED THE CALCULATIONS,
WE PROVIDE SOME VALUES OF THESE PARAMETERS FOR YOU.

WE DO NOT WARRANTY THAT THE VALUES OF THESE PARAMETERS ARE CONVERGED.

THE RESULTS PRESENTED BELOW HAVE BEEN OBTAINED USING:
Executable      : siesta
Version         : 5.1-MaX-41-g6ad11e234-dirty
Architecture    : x86_64
Compiler version: GNU-12.3.0
Compiler flags  : -fallow-argument-mismatch -O3 -march=native
Parallelisations: MPI
NetCDF support
NetCDF-4 support
NetCDF-4 MPI-IO support
Lua support
DFT-D3 support
Wannier90 wrapper support
RESULTS MIGHT DIFFER SLIGHTLY DEPENDING ON THE PLATFORM, COMPILER,
AND COMPILATION FLAGS
*******************************************************************************

All the lines after the $ should be written by the user in the command line.

* Inspect the input file, h2.fdf, and examine in detail 
the different input variables. More information can be obtained from 
the Siesta web page (http://www.icmab.es/siesta), following the link 
Documentations, and then Manual.

* Run the code for a set of different interatomic distances.
For instance, from 0.40 Ang to 3.00 Ang in steps of 0.10 Ang.
Save each output file in a separate file.

This can be done automatically using the script prepared for you or by hand.

If you chose the first case (more confortable), just type:

$ sh h2-lattice_constant.sh 0.4 0.1 3.0

In the second case, to get familiar with the input/output, type for every lattice constant

$ siesta < h2.fdf > h2.your_interatomic_distance.out

* Tabulate the total energy as a function of the interatomic distance.
In order to get this, type:

$ grep --text "Total =" h2.*.out > h2.distance.dat

* Edit the h2.distance.dat file with your favorite text editor,
and leave only two columns:
the first with the interatomic distance (in this case, in Angstroms),
and the second with the total energy (in this case, in eV).
At the end, it should look like this
(results might differ slightly depending on the platform, compiler,
and compilation flags):

0.400     -24.119850
0.500     -28.054713
0.600     -29.820680
0.700     -30.521754
0.800     -30.666948
0.900     -30.508900
1.000     -30.185812
1.100     -29.778960
1.200     -29.336826
1.300     -28.888026
1.400     -28.449301
1.500     -28.030197
1.600     -27.635848
1.700     -27.268631
1.800     -26.929245
1.900     -26.617358
2.000     -26.332054
2.100     -26.072091
2.200     -25.836015
2.300     -25.622366
2.400     -25.429569
2.500     -25.256152
2.600     -25.100767
2.700     -24.961752
2.800     -24.837601
2.900     -24.726725
3.000     -24.627523

* Plot the total energy as a function of the interatomic distance.

$ gnuplot
gnuplot> plot "h2.distance.dat" u 1:2 w l

* To produce a postscript file with the previous figure

gnuplot> set terminal postscript
gnuplot> set output "h2.distance.ps"
gnuplot> replot
gnuplot> quit

* To generate a pdf file from the previous postscript

$ ps2pdf h2.distance.ps


