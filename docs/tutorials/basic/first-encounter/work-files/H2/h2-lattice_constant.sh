
#!/bin/bash
# Check if the correct number of arguments is provided
if [ "$#" -ne 3 ]; then
  echo "Usage: sh lattice_constant.sh <start> <step> <end>"
  exit 1
fi

# Assign command-line arguments to variables
start=$1
step=$2
end=$3

# Set locale to ensure dot as decimal separator
export LC_NUMERIC="C"

for i in $(seq $start $step $end)
do
  formatted_i=$(printf "%.3f" "$i")
  cat << EOF > h2.fdf
SystemName          H2 molecule
SystemLabel         h2
NumberOfAtoms       2
NumberOfSpecies     1

%block ChemicalSpeciesLabel
 1  1  H      # Species index, atomic number, species label
%endblock ChemicalSpeciesLabel

AtomicCoordinatesFormat  Ang
%block AtomicCoordinatesAndAtomicSpecies
 0.000  0.000  0.000  1
 $formatted_i  0.000  0.000  1
%endblock AtomicCoordinatesAndAtomicSpecies

SCF.Mixer.History     3

EOF

  siesta < h2.fdf > h2.$formatted_i.out
  echo "Calculation for $formatted_i Ang complete"

done


