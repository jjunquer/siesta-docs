.. _how-to:

How-to guides
=============

.. _how-to-build-siesta:

Building Siesta
--------------------------------------------

.. toctree::
    :maxdepth: 1

    build-overview
    requirements
    build-prep-env
    build-manually
    build-esl-bundle
    build-easybuild
    build-install
    conda
    spack

.. _how-to-optimize-basis-sets:

Basis Optimization
------------------------------------

.. toctree::
    :maxdepth: 1

.. _how-to-visualize:

Visualization
-------------

.. toctree::
    :maxdepth: 1

    analysis-tools
    orbitals
    stm_utils
    dos-pdos

Other
-----

.. toctree::
    :maxdepth: 1

    flos
    

